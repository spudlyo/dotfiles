#!/usr/bin/perl
# Dumb script to launch things.

use warnings;
use strict;
use Data::Dumper;

$ENV{PATH} = '/bin:/usr/bin';
die "Need to launch something" unless $#ARGV + 1;
system(sprintf "/usr/bin/pkill -x -f '%s' >/dev/null 2>&1", join ' ', @ARGV);
sleep 1;
exec @ARGV;
