#!/bin/bash
# This script is a modified version of Suraj N. Kurapati's yank.
# https://github.com/sunaku/home/blob/master/bin/yank
#
# I fiddled with it, because that's what I do. Also, I added some
# nested tmux support that works for me.
#

put() {
    esc="$1"

    if [ -n "${SSH_TTY+foo}" ]; then
        # If we are SSH'd into a host, assume that we're running tmux
        # on the host that initiated the SSH, because that's how I roll.
        esc="\033Ptmux;\033$esc\033\\" # Wrap the message in a tmux DCS.
        if [ -n "${TMUX+foo}" ]; then
            # This means we're in a nested tmux session.  In this
            # case, we ask TMUX for our SSH_TTY because it will be set
            # properly on re-attach and won't be stale. In order for
            # this to work, we set this in our .tmux.conf:
            # set -ag update-environment "SSH_TTY"
            TTY="$(tmux show-environment SSH_TTY | cut -d'=' -f2 | tr -d '\n')"
        else
            TTY=$SSH_TTY
        fi
    else
        # We're on our local host.
        TTY=/dev/stdout
        if [ -n "${TMUX+foo}" ]; then
            esc="\033Ptmux;\033$esc\033\\" # Wrap the message in a tmux DCS.
        fi
    fi
    printf "$esc" > $TTY
}

buf=$(cat "$@")
buflen=$(printf %s "$buf" | wc -c)
maxlen=74994
if [ "$buflen" -gt "$maxlen" ]; then
   printf "input is %d bytes too long" "$(( buflen - maxlen ))" >&2
fi

# First emit a bogus base64 to tell the `kitty` terminal that this is
# a new clipboard message, and not to concatenate it to the last one.
# Other terminal programs should just ignore this.
put "\033]52;c;!\a"

# Emit the OSC 52!
put "\033]52;c;$(printf %s "$buf" | head -c $maxlen | base64 | tr -d '\r\n')\a"
