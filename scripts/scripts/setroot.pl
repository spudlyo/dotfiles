#!/usr/bin/perl
# Dumb script to cycle through wallpapers.

use warnings;
use strict;

my $PAPERS = "$ENV{HOME}/.config/wal/papers";
my $LINK   = "wallpaper";
my $WAL    = 'hsetroot -fill';

die "Bad dir: '$PAPERS'!" unless chdir($PAPERS);

my $index = 0;
my $curr  = undef;
my @walls = sort(glob("*.png"));

die "No wallpapers!" unless $#walls;

if (!defined($curr = readlink $LINK)) {
    symlink $walls[$index], $LINK;
    $curr = $walls[$index]
}

foreach my $wall (@walls) {
    if ($wall eq $curr) {
	$index = 0 if ++$index > $#walls; # wrap around
	unlink $LINK;
	symlink $walls[$index], $LINK;
	exec "$WAL  $walls[$index]";
    }
    ++$index;
}
