if [[ $TERM = dumb ]]; then
    export PS1='$ '
    return
fi

set -o physical

eval "$(/opt/homebrew/bin/brew shellenv)"

export GOPATH="$HOME/go"
export PATH=~/bin:$GOPATH/bin:$PATH
export GO111MODULE=on
export STOW_DIR="$HOME/git/dotfiles"
export GPG_TTY=$(tty)
export PYENV_VIRTUALENV_DISABLE_PROMPT=1

eval "$(gdircolors)"

export LC_CTYPE=en_US.UTF-8

shopt -s histappend
HISTCONTROL=ignoredups:erasedups
HISTSIZE=100000
CDPATH=~/.paths

export FZF_DEFAULT_OPTS='--bind ctrl-k:kill-line,ctrl-v:page-down,alt-v:page-up'
export FZF_DEFAULT_COMMAND="rg --files --no-ignore-vcs --hidden"
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

export PKG_CONFIG_PATH="$(brew --prefix)/opt/libxml2/lib/pkgconfig"
export PS1='\h:\w\$ '
export COLORTERM=truecolor

if [ -f "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh" ]; then
    export GIT_PROMPT_ONLY_IN_REPO=1
    export GIT_PROMPT_SHOW_UPSTREAM=1
    export GIT_PROMPT_SHOW_UNTRACKED_FILES=all
    export __GIT_PROMPT_DIR="$(brew --prefix)/opt/bash-git-prompt/share"
    source "$(brew --prefix)/opt/bash-git-prompt/share/gitprompt.sh"
fi

if [[ -r "$(brew --prefix)/etc/profile.d/bash_completion.sh" ]]; then
    source "$(brew --prefix)/etc/profile.d/bash_completion.sh"
fi

export EMACSPATH="$(brew --prefix)/bin"
export EDITOR="$EMACSPATH/emacsclient -a='' -t "

for file in ~/.{aliases,functions}; do
	[ -r "$file" ] && [ -f "$file" ] && source "$file";
done;

[[ $INSIDE_EMACS = vterm ]] && PS1=$PS1'\[$(vterm_prompt_end)\]'
