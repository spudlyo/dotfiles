HISTFILESIZE=100000
if [ -f ~/.bashrc ]; then
    # shellcheck source=/dev/null
    . ~/.bashrc
fi

[[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"

if command -v direnv 1>/dev/null 2>&1; then
    eval "$(direnv hook bash)"
fi

if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
    PYENV_ROOT=$(pyenv root)
    export PYENV_ROOT
fi

if command -v rbenv 1>/dev/null 2>&1; then
    eval "$(rbenv init -)"
fi

if command -v plenv 1>/dev/null 2>&1; then
    eval "$(plenv init -)"
fi
