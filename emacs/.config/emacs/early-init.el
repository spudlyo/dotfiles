(setq frame-resize-pixelwise t)

(setq default-frame-alist
       '((height . 94)
         (width . 180)
         (left . 1280)
         (top . 0)
         (vertical-scroll-bars . nil)
         (horizontal-scroll-bars . nil)
         (tool-bar-lines . 0)
         (font . "Jetbrains Mono-12")))

(setq custom-safe-themes t)
(load-theme 'modus-vivendi)
