;;; init.el --- Spudlyo's Emacs Initializtion File

;; Author: Mike Hamrick <mikeh@muppetlabs.com>

;;; Commentary:
;;
;; I put these Emacs Lisp documentation strings in here because I really,
;; really, want checkdoc.el to like me.  I crave its approval.
;;

;;; Code:
(setq user-full-name "Mike Hamrick")
(setq user-mail-address "mikeh@muppetlabs.com")

(setq gc-cons-threshold (* 8 1024 1024 1024))
(run-with-idle-timer 10 t (lambda () (garbage-collect)))
(setq read-process-output-max (* 1024 1024))

(defconst custom-file (expand-file-name "custom.el" user-emacs-directory))
(unless (file-exists-p custom-file)
  (write-region "" nil custom-file))

;; Native compile bind
(global-set-key (kbd "H-x") 'emacs-lisp-native-compile-and-load)

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(let ((default-directory (expand-file-name "lisp" user-emacs-directory)))
  (normal-top-level-add-subdirs-to-load-path))

(require 'package)
(package-initialize)
(setq package-enable-at-startup nil)
(setq package-archives ())
(setq package-native-compile t)
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(defvar my-auto-save-dir
	(file-name-as-directory (expand-file-name
				 "autosave" user-emacs-directory)))
(unless (file-exists-p my-auto-save-dir)
  (make-directory my-auto-save-dir))

(add-to-list 'auto-save-file-name-transforms
             (list "\\(.+/\\)*\\(.*?\\)" (expand-file-name "\\2" my-auto-save-dir))
             t)

(setq backup-by-copying t
      backup-directory-alist
      `((".*" . ,(expand-file-name "backups" user-emacs-directory)))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)

(require 'use-package)
(setq use-package-always-ensure t)
(setq use-package-always-defer nil)
(setq use-package-always-demand nil)
(setq use-package-expand-minimally nil)
(setq use-package-hook-name-suffix nil)
(setq use-package-compute-statistics nil)

(unless window-system
  (xterm-mouse-mode)
  (global-set-key (kbd "<mouse-4>") 'scroll-down-line)
  (global-set-key (kbd "<mouse-5>") 'scroll-up-line))

(defun my-change-window-divider ()
  "Make the vertical window divider look nicer."
  (set-display-table-slot standard-display-table 5 ?│)
  (set-window-display-table (selected-window) standard-display-table))

(global-auto-revert-mode)
(setq garbage-collection-messages t)


(add-hook 'window-configuration-change-hook 'my-change-window-divider)
(set-face-background 'vertical-border "black")
(set-face-foreground 'vertical-border "gray18")

;; UTF-8 all the things!
(define-coding-system-alias 'UTF-8 'utf-8)
(set-charset-priority 'unicode)
(setq locale-coding-system   'utf-8)
(set-terminal-coding-system  'utf-8)
(set-keyboard-coding-system  'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system        'utf-8)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))

(setq inhibit-splash-screen t)
(setq inhibit-startup-screen t)
(setq inhibit-startup-buffer-menu t)
(setq inhibit-startup-message t)
(setq inhibit-startup-echo-area-message t)
(setq ring-bell-function 'ignore)
(setq create-lockfiles nil)
(setq vc-follow-symlinks t)
(setq message-log-max 10000)
(setq debug-on-error nil)
(fset 'yes-or-no-p 'y-or-n-p)

(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)
(save-place-mode 1)
(set-fill-column 100)

(use-package epa
  :ensure nil
  :config
  (setq epg-pinentry-mode 'loopback))

(use-package pinentry
  :commands pinentry-start
  :config (setenv "INSIDE_EMACS" emacs-version)
  :hook (after-init-hook . pinentry-start))

;; Terminal mappings to support iTerm2 for macOS
(progn
  (let ((map (if nil ; (boundp 'input-decode-map)
                 input-decode-map
               function-key-map)))
    (define-key map "\e[1;P09" (kbd "H-a"))
    (define-key map "\e[1;P10" (kbd "H-b"))
    (define-key map "\e[1;P11" (kbd "H-c"))
    (define-key map "\e[1;P12" (kbd "H-d"))
    (define-key map "\e[1;P13" (kbd "H-e"))
    (define-key map "\e[1;P14" (kbd "H-f"))
    (define-key map "\e[1;P15" (kbd "H-g"))
    (define-key map "\e[1;P16" (kbd "H-h"))
    (define-key map "\e[1;P17" (kbd "H-i"))
    (define-key map "\e[1;P18" (kbd "H-j"))
    (define-key map "\e[1;P19" (kbd "H-k"))
    (define-key map "\e[1;P20" (kbd "H-l"))
    (define-key map "\e[1;P21" (kbd "H-m"))
    (define-key map "\e[1;P22" (kbd "H-n"))
    (define-key map "\e[1;P23" (kbd "H-o"))
    (define-key map "\e[1;P24" (kbd "H-p"))
    (define-key map "\e[1;P25" (kbd "H-q"))
    (define-key map "\e[1;P26" (kbd "H-r"))
    (define-key map "\e[1;P27" (kbd "H-s"))
    (define-key map "\e[1;P28" (kbd "H-t"))
    (define-key map "\e[1;P29" (kbd "H-u"))
    (define-key map "\e[1;P30" (kbd "H-v"))
    (define-key map "\e[1;P31" (kbd "H-w"))
    (define-key map "\e[1;P32" (kbd "H-x"))
    (define-key map "\e[1;P33" (kbd "H-y"))
    (define-key map "\e[1;P34" (kbd "H-z"))
    (define-key map "\e[1;P35" (kbd "S-SPC"))
    (define-key map "\e[1;P36" (kbd "C-M-m"))
    (define-key map "\e[1;P37" (kbd "C-,"))
    (define-key map "\e[1;P38" (kbd "C-."))
    (define-key map "\e[1;P39" (kbd "C-;"))
))

(use-package exec-path-from-shell
  :commands exec-path-from-shell-initialize
  :if (string-equal system-type "darwin")
  :config
  (exec-path-from-shell-initialize))

(use-package all-the-icons
  :commands
  all-the-icons-insert all-the-icons-insert-faicon all-the-icons-insert-fileicon
  all-the-icons-insert-material all-the-icons-insert-octicon all-the-icons-insert-wicon
  all-the-icons-icon-for-dir all-the-icons-icon-for-file all-the-icons-icon-for-mode
  all-the-icons-icon-for-url all-the-icons-icon-family all-the-icons-icon-family-for-buffer
  all-the-icons-icon-family-for-file all-the-icons-icon-family-for-mode
  all-the-icons-icon-for-buffer all-the-icons-faicon all-the-icons-octicon
  all-the-icons-fileicon all-the-icons-material all-the-icons-wicon
  all-the-icons-default-adjust all-the-icons-color-icons all-the-icons-scale-factor
  all-the-icons-icon-alist all-the-icons-dir-icon-alist all-the-icons-weather-icon-alist
  all-the-icons-icon-for-dir-with-chevron)

(use-package dired
  :ensure nil
  :bind ("H-a" . dired)
  :config
  (when (string= system-type "darwin")
    (setq dired-use-ls-dired t
          insert-directory-program "/opt/homebrew/bin/gls"
          dired-listing-switches "-aBhl --group-directories-first"))
  (use-package all-the-icons-dired
    :commands all-the-icons-dired-mode
    :hook (dired-mode-hook . all-the-icons-dired-mode)))

(use-package ibuffer
  :ensure nil
  :bind (("C-x C-b" . ibuffer)
         :map ibuffer-mode-map
         ("C-g" . quit-window))
  :custom
  (ibuffer-expert t))

(use-package all-the-icons-ibuffer
  :commands all-the-icons-ibuffer-mode
  :hook
  (after-init-hook . all-the-icons-ibuffer-mode))

(defun my-fix-icons ()
  "Hack around `display-graphic-p' things."
  (interactive)
  (let ((rich (car(find-definition-noselect 'all-the-icons-ivy-rich--format-icon nil)))
        (ibuf (car(find-definition-noselect 'all-the-icons-ibuffer-icon-face 'defface))))
    (mapcar
     (lambda (buf)
       (with-current-buffer buf
         (goto-char (point-min))
         (while (search-forward "(display-graphic-p)" nil t)
           (replace-match "t"))
         (save-buffer)
         (emacs-lisp-native-compile-and-load)
         (kill-buffer))) (list rich ibuf))))

(defun my-start-frame (frame)
  "Some default frame configuration, passed in FRAME."
  (interactive)
  (with-selected-frame frame
    (when (daemonp)
      ;; In daemon mode, let's not exit Emacs.
      ;; Deleting the frame is almost always what I want.
      (global-set-key (kbd "C-x C-c") 'delete-frame)
      ;;(set-face-attribute 'fringe nil :background nil)
      (message "Frame start complete."))))
(add-to-list 'after-make-frame-functions #'my-start-frame)

(use-package emacs
  :commands my-crm-indicator
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; Alternatively try `consult-completing-read-multiple'.
  (defun my-crm-indicator (args)
    (cons (concat "[CRM] " (car args)) (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'my-crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  (setq read-extended-command-predicate
        #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t)
  :bind (("s-<return>" . toggle-frame-fullscreen)
         ("C-<return>" . toggle-frame-fullscreen))
  :custom
  (xterm-extra-capabilities '(setSelection)))

(use-package recentf
  :init
  (setq recentf-auto-cleanup 'never)
  (recentf-mode 1))

(use-package keycast)
(use-package diminish)
(use-package delight)

(use-package doom-modeline
  :custom
  (doom-modeline-project-detection 'projectile)
  (doom-modline-unicode-fallback t)
  (doom-modeline-indent-info t)
  (doom-modeline-vcs-max-length 30)
  :config
  (add-to-list 'after-make-frame-functions
               (lambda (frame)
                 (with-selected-frame frame
                   (if (memq window-system '(mac ns))
                       (setq doom-modeline-icon t)
                     (setq doom-modeline-icon t)))))
  :hook (after-init-hook . doom-modeline-mode))

(use-package doom-themes
  :commands
  doom-themes-visual-bell-config
  doom-themes-org-config
  :custom
  (doom-themes-enable-bold t)
  (doom-themes-enable-italic t)
  :config
  ; (load-theme 'doom-spacegrey t)
  (doom-themes-visual-bell-config)
  (doom-themes-org-config))

(use-package neotree
  :custom
  (neo-theme 'icons
  (neo-window-fixed-size nil)))

(use-package treemacs
  :commands
  treemacs treemacs-edit-workspaces treemacs-load-theme
  :bind (("C-c C-t" . #'treemacs))
  :config
  (use-package treemacs-all-the-icons)
  (treemacs-load-theme "all-the-icons"))

(use-package tramp
  :commands tramp-set-completion-function
  :config
  (setq tramp-default-method "ssh")
  (tramp-set-completion-function
   "ssh"
   '((tramp-parse-sconfig "/etc/ssh_config")
     (tramp-parse-sconfig "~/.ssh/config"))))

(use-package iedit
  :bind (("C-\\" . iedit-mode)))

(use-package indent-tools
  :init
  (global-set-key (kbd "C-c >") 'indent-tools-hydra/body))

;;;; Org-Mode
(use-package org-bullets
  :commands org-bullets-mode
  :init
  (add-hook 'org-mode-hook #'org-bullets-mode))

(use-package ox-gfm :commands org-gfm-publish-to-gfm)
(require 'org-mouse)
(require 'org-tempo)

(use-package org
  :commands
  org-indent-mode org-babel-lob-ingest org-html-publish-to-html
  org-publish org-ascii-publish-to-ascii org-babel-do-load-languages
  :init
  (defvar my-org-dir (expand-file-name "~/org"))
  (defvar my-org-publish-dir (expand-file-name "/pay/src/zoolander/src/python/pytask/auditboard/docs"))
  (defun my-maybe-lob-ingest ()
    "Maybe I should lob-ingest the contents of this buffer?"
    (if (and buffer-file-name
             (string-match
              (format "%s/.*code\\.inc$" my-org-dir)
              buffer-file-name))
        (org-babel-lob-ingest buffer-file-name)))
  (defun my-org-mode-hook ()
    "Stuff to do when entering org mode."
    (setq org-fontify-done-headline t)
    (set-face-attribute 'org-done nil :strike-through t)
    (set-face-attribute 'org-headline-done nil
                        :strike-through t
                        :foreground "light gray")
    (my-maybe-lob-ingest)
    (turn-on-auto-fill)
    (org-indent-mode 1)
    (setq fill-column 80))
  (defun my-git-publish (ppl)
    "Run a shell script if it's there."
    (let ((publish-script (format "%s/publish.sh" my-org-publish-dir)))
      (when (file-executable-p publish-script)
          (start-process-shell-command "pub" nil publish-script))))
  (defun my-org-confirm-babel-evaluate (lang body)
    "Don't confirm squat."
    (not (member lang '("sh" "python" "emacs-lisp" "ruby" "shell" "dot" "scheme"
                        "perl" "elisp"))))
  (defun my-publish (a b c)
    "Here is how I like to publish."
    (setq org-export-with-toc t)
    (org-gfm-publish-to-gfm a b c))
  :config
  (setq org-directory my-org-dir
        org-startup-indented t
        org-babel-min-lines-for-block-output 0
        org-startup-folded "showeverything"
        org-startup-with-inline-images t
        org-src-preserve-indentation t
        org-use-speed-commands t
        org-hide-emphasis-markers t
        org-export-with-section-numbers nil
        org-export-with-toc t
        org-export-with-date nil
        org-export-time-stamp-file nil
        org-export-with-email t
        org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate
        org-babel-default-header-args
          (cons '(:noweb . "yes")
                (assq-delete-all :noweb org-babel-default-header-args))
        org-babel-default-header-args
          (cons '(:exports . "both")
                (assq-delete-all :exports org-babel-default-header-args))
        org-babel-default-header-args
          (cons '(:results . "output verbatim replace")
                (assq-delete-all :results org-babel-default-header-args))
        org-ellipsis " ▼ "
        org-publish-project-alist
        `(("auditboard"
           :base-directory "~/org/auditboard"
           :publishing-directory ,my-org-publish-dir
           :publishing-function my-publish
           :completion-function (my-git-publish))))
  (custom-set-faces '(org-ellipsis ((t (:foreground "gray40" :underline nil)))))
  (global-set-key (kbd "C-c c") 'org-capture)
  (global-set-key (kbd "C-c l") 'org-store-link)
  (global-set-key (kbd "C-c a") 'org-agenda)
  (org-babel-do-load-languages 'org-babel-load-languages
   '((shell      . t)
     (python     . t)
     (ruby       . t)
     (perl       . t)
     (emacs-lisp . t)
     (scheme     . t)))
  :bind (("M-p" . #'org-publish))
  :hook
  (after-save-hook . my-maybe-lob-ingest)
  (org-mode-hook . my-org-mode-hook))

(use-package memoize)

(use-package virtualenvwrapper
  :commands
  venv-initialize-interactive-shells venv-workon
  :config
  (venv-initialize-interactive-shells))

(use-package projectile
  :commands projectile-mode projectile-project-name
  :config
  (setq projectile-project-search-path '("~/git"))
  (setq projectile-use-git-grep t)
  (setq projectile-current-project-on-switch 'move-to-end)
  (setq projectile-mode-line-prefix " Proj")
  (setq projectile-completion-system 'ivy)
  :bind (:map projectile-mode-map
        ("H-p"   . projectile-command-map)
        ("C-c p" . projectile-command-map)
        :map projectile-command-map
        ("t" . projectile-test-project)
        ("r" . projectile-run-project))
  :hook
  (after-init-hook . projectile-mode))

(use-package xterm-color)

(setq compilation-environment '("TERM=xterm-256color"))
(setq compilation-read-command nil)
(setq compilation-scroll-output t)
(defun my/advice-compilation-filter (f proc string)
  (funcall f proc (xterm-color-filter string)))
(advice-add 'compilation-filter :around #'my/advice-compilation-filter)

(use-package ibuffer-projectile
  :commands ibuffer-projectile-set-filter-groups ibuffer-do-sort-by-alphabetic
  :hook
  (ibuffer-hook . (lambda ()
                    (ibuffer-projectile-set-filter-groups)
                    (unless (eq ibuffer-sorting-mode 'alphabetic)
                      (ibuffer-do-sort-by-alphabetic)))))

(use-package counsel-projectile
  :disabled t
  :commands counsel-projectile-mode
  :config
  (counsel-projectile-mode))

;;;; Version Control
(use-package magit
  :bind (("C-x g" . magit-status)))

(use-package git-gutter
  :disabled t
  :delight
  :hook
  (prog-mode-hook . git-gutter-mode)
  (org-mode-hook  . git-gutter-mode))

;;;; Flycheck & Flyspell

(use-package flycheck
  :commands global-flycheck-mode
  :config
  (setq flycheck-emacs-lisp-load-path load-path)
  (setq-default flycheck-disabled-checkers
		(append flycheck-disabled-checkers
			'(emacs-lisp-checkdoc)
			'(ruby-rubylint)
			'(yaml-ruby)
			'(yaml-jsyaml)
			'(json-jsonlint)
			'(puppet-lint)
			'(json-python-json)))
  :hook
  (after-init-hook . global-flycheck-mode))

(use-package flycheck-yamllint
  :hook (flycheck-mode-hook . flycheck-yamllint-setup))

(use-package flyspell
  :disabled t
  :commands flyspell-mode
  :config
  (setq ispell-program-name "aspell"
        ispell-extra-args '("--sug-mode=ultra"))
  :hook
  ('text-mode-hook . 'flyspell-mode)
  ('org-mode-hook  . 'flyspell-mode))

(use-package yaml-mode
 :mode ("\\.yml$" . yaml-mode))

(use-package company
  :delight
  :commands global-company-mode
  :bind ("M-<tab>" . company-begin-commands)
  :hook (after-init-hook . global-company-mode)
  :config
  (define-key company-active-map (kbd "C-n") 'company-select-next-or-abort)
  (define-key company-active-map (kbd "C-p") 'company-select-previous-or-abort)
  (setq company-backends '(company-capf company-files company-dabbrev))
  (setq company-transformers '(company-sort-by-occurrence))
  (setq company-tooltip-limit 30)
  (setq company-idle-delay 2) ; Trigger company manually
  (setq company-echo-delay 0))

(use-package json-mode)

(use-package lsp-mode
  :config
  (setq lsp-enable-snippet nil)
  (setq lsp-enable-file-watchers nil)
  ; (setq lsp-file-watch-threshold 60000)
  (setq lsp-response-timeout 120)
  :custom
  (lsp-log-max t)
  :hook
  (ruby-mode-hook . lsp)
  (lua-mode-hook  . lsp)
  (go-mode-hook   . lsp)
  (js-mode-hook   . lsp)
  (c-mode-hook    . lsp)
  (json-mode-hook . (lambda () (setq lsp-enabled-clients '(json-ls)) (lsp)))
  :commands (lsp lsp-deferred))

(use-package lsp-ui
  :commands
  lsp-ui-doc-mode
  lsp-ui-doc--hide-frame
  :custom
  (lsp-ui-sideline-enable nil)
  (lsp-ui-doc-enable nil)
  (lsp-doc-header t)
  (lsp-ui-doc-position 'at-point)
  (lsp-ui-doc-max-width 120)
  (lsp-ui-doc-max-height 30)
  (lsp-ui-doc-use-childframe nil)
  (lsp-ui-doc-use-webkit nil)
  (lsp-ui-flycheck-enable nil)
  (lsp-ui-imenu-enable t)
  (lsp-ui-imenu-kind-position 'top)
  (lsp-ui-peek-enable t)
  (lsp-ui-peek-peek-height 20)
  (lsp-ui-peek-list-width 50)
  (lsp-ui-peek-fontify 'on-demand)
  :preface
  (defun my-toggle-lsp-ui-doc ()
    (interactive)
    (if lsp-ui-doc-mode
        (progn
          (lsp-ui-doc-mode -1)
          (lsp-ui-doc--hide-frame))
      (lsp-ui-doc-mode 1)))
  :bind
  (:map lsp-mode-map
	("C-c C-r" . lsp-ui-peek-find-references)
	("C-c C-j" . lsp-ui-peek-find-definitions)
	("C-c i"   . lsp-ui-peek-find-implementation)
	("C-c m"   . lsp-ui-imenu)
	("C-c s"   . lsp-ui-sideline-mode)
	("C-c d"   . my-toggle-lsp-ui-doc))
  :hook
  (lsp-mode-hook . lsp-ui-mode))

(use-package lsp-pyright
  :commands lsp-diagnostics-flycheck-enable
  :hook
  (boring-mode-hook . (lambda () (require 'lsp-pyright) (lsp)))
  (python-mode-hook . (lambda () (require 'lsp-pyright) (lsp)))
  :custom
  (lsp-pyright-extra-major-modes '(boring-mode))
  :init (when (executable-find "python3")
          (setq lsp-pyright-python-executable-cmd "python3")))

(use-package eval-in-repl
  :hook
  (python-mode-hook . (lambda () (local-set-key (kbd "C-c r") 'eir-eval-in-python))))

(use-package python
  :init
  (setq-default indent-tabs-mode nil)
  :mode
  ("\\.py\\'" . python-mode)
  ("\\.wsgi$" . python-mode)
  :interpreter ("python" . python-mode)
  :config
  (setq python-indent-offset 4)
  (setq python-indent-guess-indent-offset-verbose nil))

(use-package blacken
  :custom
  (blacken-only-if-project-is-blackened t)
  :hook
  (python-mode-hook . blacken-mode))

(use-package python-pytest
  :bind (("C-c t" . python-pytest-dispatch)))

(use-package python-coverage)

(use-package macrostep
  :config
  (define-key emacs-lisp-mode-map (kbd "C-c e") 'macrostep-expand))

(use-package go-mode
  :init
  :config
  (defun my-go-mode-hook ()
    "My wacky go mode hook."
    (setq gofmt-command "goimports")
    (setq godoc-and-godef-command "go doc")
    (add-hook 'before-save-hook 'gofmt-before-save)
    (if (not (string-match "go" compile-command))
	(set (make-local-variable 'compile-command)
	     "go build -v"))
    (setq tab-width 4))
  (add-hook 'go-mode-hook 'my-go-mode-hook)
  (add-hook 'before-save-hook 'delete-trailing-whitespace))

(use-package web-mode
  :config
  (setq
   web-mode-code-indent-offset 2
   web-mode-comment-style 2
   web-mode-css-indent-offset 2
   web-mode-enable-current-element-highlight t
   web-mode-enable-current-column-highlight t
   web-mode-markup-indent-offset 2)
  :mode
  ("\\.erb\\'" . web-mode)
  ("\\.html?\\'" . web-mode)
  ("\\.tpl\\'" . web-mode))

(use-package flycheck-golangci-lint
  :config
  (setq flycheck-golangci-lint-config (expand-file-name "~/.golangci.yaml"))
  :hook (go-mode-hook . flycheck-golangci-lint-setup))

(use-package lispy)

(use-package which-key
  :commands which-key-mode
  :hook (after-init-hook . which-key-mode))

(use-package tree-sitter
  :hook
  (after-init-hook . global-tree-sitter-mode)
  (tree-sitter-after-on-hook . tree-sitter-hl-mode))

(use-package tree-sitter-langs)

(use-package vterm
  :commands vterm-send-next-key
  :bind (:map vterm-mode-map
              ("C-\\" . nil)
              ("C-q"  . vterm-send-next-key))
  :custom
  ;;(vterm-shell "tmux new -A -D -s `emacs-hostname -s`")
  (vterm-shell "/opt/homebrew/bin/bash")
  (vterm-always-compile-module t))

(use-package popper
  :ensure t ; or :straight t
  :bind (("C-\\"   . popper-toggle-latest)
         ("M-\\"   . popper-cycle)
         ("C-M-\\" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          "\\*xref\\*"
          help-mode
          compilation-mode))
  (setq popper-reference-buffers
      (append popper-reference-buffers
              '("^\\*eshell.*\\*$" eshell-mode ;eshell as a popup
                "^\\*shell.*\\*$"  shell-mode  ;shell as a popup
                "^\\*term.*\\*$"   term-mode   ;term as a popup
                "^\\*vterm.*\\*$"  vterm-mode  ;vterm as a popup
                )))
  (setq popper-window-height 20)
  (popper-mode +1)
  (popper-echo-mode +1))

(use-package vertico
  :init
  (vertico-mode))

(use-package orderless
  :config
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

(use-package savehist
  :hook (after-init-hook . savehist-mode))

(use-package embark
  :bind
   (("C-."   . embark-act)
    ("C-;"   . embark-dwim)
    ("C-h B" . embark-bindings)))

(use-package all-the-icons-completion
  :hook (after-init-hook . all-the-icons-completion-mode))

(use-package marginalia
  :hook (after-init-hook . marginalia-mode))

(use-package vertico)

(use-package consult
  :bind
  (("C-x b" . consult-buffer)
   ("M-r"   . consult-ripgrep)))

(use-package profiler
  :config
  (setf (caar profiler-report-cpu-line-format) 80
      (caar profiler-report-memory-line-format) 80))

(use-package sudo-edit)

;;; init.el ends here
