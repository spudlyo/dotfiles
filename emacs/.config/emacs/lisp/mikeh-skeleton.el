;;; mikeh-skeleton.el --- My skeletons    -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Mike Hamrick

;; Author: Mike Hamrick <mikeh@muppetlabs.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is where I keep the skeleton templates I use.  They look
;; pretty ugly which is why I keep them stashed here in a separate
;; file.

;;; Code:

(require 'skeleton)

(define-skeleton mikeh-elisp-skeleton
  "Skeleton to use when creating a new Emacs Lisp Package"
  "Short description: "
  ";;; " (file-name-nondirectory (buffer-file-name)) " --- " str
  (make-string (max 2 (- 80 (current-column) 27)) ?\s)
  "-*- lexical-binding: t; -*-" '(setq lexical-binding t)
  "

;; Copyright (C) " (format-time-string "%Y") "  "
 (getenv "ORGANIZATION") | (progn user-full-name) "

;; Author: " (user-full-name)
'(if (search-backward "&" (line-beginning-position) t)
     (replace-match (capitalize (user-login-name)) t t))
'(end-of-line 1) " <" (progn user-mail-address) ">
;; Created: " (format-time-string "%d %b %Y") "
;; Version: 0.0.1
;; Package-Requires: ((emacs \"24.1\"))
;; Keywords: "
 '(require 'finder)
 ;;'(setq v1 (apply 'vector (mapcar 'car finder-known-keywords)))
 '(setq v1 (mapcar (lambda (x) (list (symbol-name (car x))))
		   finder-known-keywords)
	v2 (mapconcat (lambda (x) (format "%12s:  %s" (car x) (cdr x)))
	   finder-known-keywords "\n"))
 ((let
      ((minibuffer-help-form v2))
    (completing-read "Keyword, C-h: " v1 nil t))
  str ", ")
 & -2 "
;; URL: https://github.com/spudlyo/" (file-name-sans-extension
                                      (file-name-nondirectory
                                       (buffer-file-name))) "

\;; This program is free software; you can redistribute it and/or modify
\;; it under the terms of the GNU General Public License as published by
\;; the Free Software Foundation, either version 3 of the License, or
\;; (at your option) any later version.

\;; This program is distributed in the hope that it will be useful,
\;; but WITHOUT ANY WARRANTY; without even the implied warranty of
\;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\;; GNU General Public License for more details.

\;; You should have received a copy of the GNU General Public License
\;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

\;;; Commentary:

\;; " _ "

\;;; Code:



\(provide '"
       (file-name-base (buffer-file-name))
       ")
\;;; " (file-name-nondirectory (buffer-file-name)) " ends here\n")

(global-set-key (kbd "C-c s") 'mikeh-elisp-skeleton)

(provide 'mikeh-skeleton)
;;; mikeh-skeleton.el ends here
