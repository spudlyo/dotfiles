set -o physical

export EDITOR='emacsclient -a= -t'
export PS1=$'\\h:\\w\\$ '
export GPG_TTY=$(tty)
export GPGHOME=~/.gnupg
export GPG_AGENT_INFO=$(gpgconf --list-dir agent-socket):0:1
export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/ssh-agent.socket

# I mess these up a lot.
alias grpe='grep'
alias emasc='emacsclient -a= -t'
alias gpt='gpg'

alias controlkill='for s in $(ls ~/.ssh/control); do ssh -O exit $s; done'
alias controlcheck='for s in $(ls ~/.ssh/control); do ssh -O check $s; done'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ls='ls --color=auto'

shopt -s histappend
HISTCONTROL=ignoredups:erasedups
HISTSIZE=100000
CDPATH=~/.paths

if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
    GIT_PROMPT_ONLY_IN_REPO=1
    source $HOME/.bash-git-prompt/gitprompt.sh
fi

loose()
{
    local OPTIND OPTARG findcmd start field dopretty
    dopretty=false
    while getopts "a" flag; do
	case $flag in
	    a) dopretty=true; shift;;
	    *) echo "Unknown flag."; return 1;
        esac
    done

    # This is if we find ourselves in a bare repo.
    if [ -d '.git/objects' ]; then
	start='.git/objects';
	field=3
    elif [ -d 'objects' ]; then
	start='objects';
	field=2
    fi

    findcmd="find $start -type f -exec /bin/ls -tr {} +"
    for f in $($findcmd | cut -d/ -f$field- | grep -Ev '^(pack|info)' | tr -d /); do
	local type size pretty
	type=$(git cat-file -t "$f");
	size=$(git cat-file -s "$f");
	if [ $dopretty = true ]; then
	    pretty=$(git cat-file -p "$f" | perl -pe 's/\n/\n\t/g');
	fi
	printf "%s: %s (%s)\n\t%s\r" "$f" "$type" "$size" "$pretty"
    done
}

emacs()
{
    if [ "$#" == "0" ]; then
	emacsclient -a= -t
    else
	emacsclient -a= -t "$@"
    fi
}

gmacs()
{
    if [ "$#" == "0" ]; then
	emacsclient -nc -a= "$HOME/txt/org"
    else
	emacsclient -nc -a= "$@"
    fi
}

goto()
{
    local OPTIND OPTARG
    local TMUXCMD='LANG="en_US.UTF-8" tmux set-option -g prefix C-o \; new-session -A -D -s'

    while getopts "n" flag; do
	case "$flag" in
	    n) TMUXCMD=""; shift;;
	    *) echo "Unknown flag."; return 1;
	esac
    done

    if [ "$#" == "0" ]; then
	echo 'Need someplace to go.'
	return 1;
    fi

    while (( "$#" )); do
	tmux new-window -n "$1" ssh -tt -q "$1" "$TMUXCMD $1"
	shift
    done
}

_goto_show()
{
    local cur hosts

    hosts=$( command perl -ne 'next unless /^host\s+(.*)/;print"$1\n" unless $1 eq "*";' ~/.ssh/config )
    cur="${COMP_WORDS[COMP_CWORD]}"
    COMPREPLY=( $(compgen -W "$hosts" "$cur") )
}
complete -F _goto_show goto
