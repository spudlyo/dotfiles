export PYENV_ROOT="$HOME/.pyenv"
export RBENV_ROOT="$HOME/.rbenv"
export RBBUILD_ROOT="$HOME/.rbenv/plugins/ruby-build"

export PATH=$RBBUILD_ROOT/bin:$RBENV_ROOT/bin:$PYENV_ROOT/bin:$PATH
export PATH=$HOME/bin:$HOME/scripts:$PATH

if command -v rbenv 1>/dev/null 2>&1; then
    eval "$(rbenv init -)"
fi

if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi
